package com.company.hotchatcmdline;

import com.google.protobuf.ByteString;
import com.rabbitmq.client.AMQP;
import com.rabbitmq.client.Consumer;
import com.rabbitmq.client.DefaultConsumer;
import com.rabbitmq.client.Envelope;

import java.io.IOException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeFormatter;
import java.util.Scanner;
import java.util.StringTokenizer;

import com.company.hotchatcmdline.MensagemOuterClass.Mensagem;

public class Main {

    public static void help(){
        System.out.println("Comandos válidos:\n\t!addUser\n\t!delUser\n\t!addGroup\n\t!delGroup\n\t!exit");
    }

    public static Mensagem buildMessage(String sender, String group,
                                        String fileName, String fileType,
                                        String content){

        DateTimeFormatter df = DateTimeFormatter.ofPattern("dd/MM/yyyy");
        DateTimeFormatter tf = DateTimeFormatter.ofPattern("HH:mm");

        Mensagem.Builder msg = Mensagem.newBuilder();

        msg.setSender(sender);
        msg.setGroup(group);
        msg.setDate(df.format(LocalDate.now()));
        msg.setTime(tf.format(LocalTime.now()));

        Mensagem.Conteudo.Builder cnt = Mensagem.Conteudo.newBuilder();

        cnt.setName(fileName);
        cnt.setType(fileType);
        cnt.setContent(ByteString.copyFrom(content.getBytes()));

        msg.addContent(cnt);

        return msg.build();
    }


    public static void main(String[] args) {

        System.out.print("Username: ");
        Scanner scan = new Scanner(System.in);
        String username = scan.nextLine();
        String destiny = null;
        String message = null;
        boolean isGroup = false;

        try {
            ClientConnect clientConnect = new ClientConnect();

            clientConnect.getChannel().queueDeclare(username, false, false, false, null);

            Consumer consumer = new DefaultConsumer(clientConnect.getChannel()) {
                @Override
                public void handleDelivery(String consumerTag, Envelope envelope, AMQP.BasicProperties properties, byte[] body)
                        throws IOException {

                    Mensagem msg = Mensagem.parseFrom(body);
                    if(msg.getGroup().isEmpty())
                        System.out.println("("  + msg.getDate()+" às "+msg.getTime()+") "
                                                + msg.getSender()+" diz: "
                                                + new String(msg.getContent(0).getContent().toByteArray()));
                    else {

                        // Trata a exibição da mensagem no mesmo console do usuário que a enviou num grupo
                        StringTokenizer stringTokenizer = new StringTokenizer(msg.getSender(), "/");
                        while (stringTokenizer.hasMoreElements()) {
                            String sender = stringTokenizer.nextElement().toString();
                            if (!sender.equalsIgnoreCase(username))
                                System.out.println("("  + msg.getDate() + " às " + msg.getTime() + ") "
                                                        + msg.getSender() + " diz: "
                                                        + new String(msg.getContent(0).getContent().toByteArray()));
                            else
                                System.out.println();
                        }
                    }

                }
            };

            clientConnect.getChannel().basicConsume(username, true, consumer);

            while(true){

                if(destiny == "" || destiny == null)
                    System.out.print(">> ");
                else if (!isGroup)
                    System.out.print(destiny+">> ");
                else
                    System.out.print(destiny+"*>> ");

                message = scan.nextLine();

                if (message.startsWith("@")) {
                    destiny = message.substring(1);
                    isGroup = false;
                    continue;
                } else if (message.startsWith("#")) {
                    destiny = message.substring(1);
                    isGroup = true;
                    continue;
                } else if (message.startsWith("!")) {

                    StringTokenizer stringTokenizer = new StringTokenizer(message, " ");
                    while (stringTokenizer.hasMoreElements()) {

                        String command = stringTokenizer.nextElement().toString();
                        String arg1 = stringTokenizer.nextElement().toString();

                        switch (command) {
                            case "!addGroup":
                                clientConnect.getChannel().exchangeDeclare(arg1, "fanout");
                                clientConnect.getChannel().queueBind(username, arg1, "");
                                break;
                            case "!addUser":
                                String arg2 = stringTokenizer.nextElement().toString();
                                clientConnect.getChannel().queueBind(arg1, arg2, "");
                                break;
                            case "!exit":
                                System.exit(0);
                                break;
                            case "!delUser":
                                String arg3 = stringTokenizer.nextElement().toString();
                                clientConnect.getChannel().queueUnbind(arg1, arg3, "");
                                break;
                            case "!delGroup":
                                clientConnect.getChannel().exchangeDelete(arg1);
                                break;
                            default:
                                System.out.println("Comando inválido!");
                                help();
                        }
                    }
                } else if (!message.isEmpty() && destiny != "" &&  destiny != null) {
                    if (!isGroup) {

                        clientConnect.getChannel().basicPublish("", destiny, null,
                                buildMessage(username, "", "", "text/plain", message).toByteArray());
                    } else {

                        clientConnect.getChannel().basicPublish(destiny, "", null,
                                buildMessage(username, destiny, "", "text/plain", message).toByteArray());
                    }
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}